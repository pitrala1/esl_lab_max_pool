#include "xparameters.h"
#include "platform.h"
#include "xil_printf.h"
#include "AxiFifoMM.h"
#include "TimeMeasure.h"

int main() {
    init_platform();
    xil_printf("Hello World\r\n");
    cleanup_platform();


    AxiFifoMM fifo(XPAR_AXI_FIFO_0_DEVICE_ID);
    constexpr unsigned data_words_num = 64;
    std::vector<int32_t> in_buff = {1, 0, 1, 5, 2, 10,
						              5, 11, 32, 1, 1, 0,
						              76, 43, 12, 1, 1, 1,
						              54, 21, 56, 7, 8, 11,
						              1, 45, 65, 23, 11, 43,
						              12, 21, 12, 53, 76, 23,
						              65, 86, 4, 2, 6, 2,
						              2, 84, 35, 23, 23, -53};


    fifo.write(in_buff);
    auto out_buff = fifo.read();
    for (unsigned i = 0; i < out_buff.size(); i++) {
        xil_printf("out_buff[%d] = %d\n\r", i, out_buff[i]);
    }



    return 0;
}
