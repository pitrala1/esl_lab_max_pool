from myhdl import block, always_seq, Signal, intbv, enum, instances


@block
# def my_sum(clk, reset, axis_s_raw, axis_m_sum):
def max_pool(clk, reset, axis_s_in, axis_m_out, width, height, depth, window_width, window_height):
    state_t = enum('ACCUMULATE', 'DUMP', 'FAULT')
    state = Signal(state_t.ACCUMULATE)

    out_width = int(width / window_width)
    out_height = int(height / window_height)
    in_size = width * height * depth
    out_size = out_width * out_height * depth
    bitwidth = 32
    output_buffer = [Signal(intbv(-7)[bitwidth:].signed()) for x in range(out_size)]
    for index in range(len(output_buffer)):
        output_buffer[index].driven = True

    output_counter = Signal(intbv(0)[bitwidth:])
    input_counter = Signal(intbv(0)[bitwidth:])
    window_counter = Signal(intbv(0)[bitwidth:])
    height_counter = Signal(intbv(0)[bitwidth:])
    width_counter = Signal(intbv(0)[bitwidth:])

    @always_seq(clk.posedge, reset=reset)
    def sum_proc():
        if state == state_t.ACCUMULATE:
            axis_m_out.tlast.next = 0
            axis_m_out.tvalid.next = 0
            if input_counter != in_size:
                axis_s_in.tready.next = 1
                if axis_s_in.tvalid == 1:
                    if axis_s_in.tdata.signed() > output_buffer[output_counter + window_counter]:
                        output_buffer[output_counter + window_counter].next = axis_s_in.tdata.signed()
                    else:
                        output_buffer[output_counter + window_counter].next = output_buffer[output_counter + window_counter]
                    # print(input_buffer[window_counter].next)
                    if width_counter != (window_width - 1):
                        width_counter.next = width_counter + 1
                    else:  # width_counter == (out_width - 1)
                        width_counter.next = 0

                        if window_counter != (out_width - 1):
                            window_counter.next = window_counter + 1
                        else:  # window_counter == (out_width - 1):
                            window_counter.next = 0

                            if height_counter != (window_height - 1):
                                height_counter.next = height_counter + 1
                            else:
                                height_counter.next = 0
                                output_counter.next = output_counter + out_width
                    if input_counter == (in_size - 1) and axis_s_in.tlast == 1:
                        state.next = state_t.DUMP
                    elif input_counter == (in_size - 1) and axis_s_in.tlast != 1:
                        state.next = state_t.FAULT
                        print("Fault state")
                    else:
                        input_counter.next = input_counter + 1

            # print(output_buffer)
            # print("-----------")
        elif state == state_t.DUMP:
            if axis_m_out.tready == 1:
                axis_m_out.tvalid.next = 1
                axis_m_out.tlast.next = 0
                axis_m_out.tdata.next = output_buffer[0]
                for index in range(out_size - 1):
                    output_buffer[index].next = output_buffer[index + 1]
                #output_buffer[-1].next = 0
                output_counter.next = output_counter - 1

                if output_counter == 1:
                    state.next = state_t.ACCUMULATE
                    axis_m_out.tlast.next = 1

    return instances()
