



if __name__ == "__main__":
    x = 6
    y = 8
    z = 6

    downsample_x = 6
    downsample_y = 4

    size_x = int(x/downsample_x)
    size_y = int(y/downsample_y)

    matrix = [[[_1*_2*_3 for _1 in range(x)] for _2 in range(y)] for _3 in range(z)]
    output = [[[] for _2 in range(size_y)] for _3 in range(z)]
    i = 0
    for matrix2d in matrix:
        of = 0
        for row in range(len(matrix2d))[0::downsample_y]:
            slice = [[] for _1 in range(size_x)]
            for of in range(size_x):
                for row_x in range(downsample_y):
                    slice[of].extend(matrix2d[row+row_x][of*downsample_x:(of*downsample_x)+downsample_x])

            for sub in slice:
                m = max(sub)
                output[i][int(row/downsample_y)].append(m)
        i += 1

    for row in output:
        print(row)

#  0 1 2 3 4 5
#  0 1 2 3 4 5
#  0 1 2 3 4 5
#  0 1 2 3 4 5
#  0 1 2 3 4 5
#  0 1 2 3 4 5


#  1 3 5
#  1 3 5