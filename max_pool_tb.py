from myhdl import block, instance, Signal, ResetSignal, StopSimulation, instances, delay,intbv
import os

from axi.axis import Axis
from myhdl_sim.clk_stim import clk_stim
from max_pool import max_pool


@block
def testbench(vhdl_output_path=None):

    reset = ResetSignal(0, active=0, async=False)
    clk = Signal(bool(0))

    axis_raw = Axis(32)
    axis_sum = Axis(32)

    clk_gen = clk_stim(clk, period=10)

    @instance
    def reset_gen():
        reset.next = 0
        yield delay(54)
        yield clk.negedge
        reset.next = 1

    @instance
    def write_stim():
        values = [1, 0, 1, 5, 2, 10,
                  5, 11, 32, 1, 1, 0,
                  76, 43, 12, 1, 1, 1,
                  54, 21, 56, 7, 8, 11,
                  1, 45, 65, 23, 11, 43,
                  12, 21, 12, 53, 76, 23,
                  65, 86, 4, 2, 6, 2,
                  2, 84, 35, 23, 23, -53]
        i = 0
        yield reset.posedge
        while i < len(values):
            yield clk.negedge
            axis_raw.tvalid.next = 1
            axis_raw.tdata.next = intbv(values[i])[32:].unsigned()
            if i == len(values) - 1:
                axis_raw.tlast.next = 1
            else:
                axis_raw.tlast.next = 0
            if axis_raw.tready == 1:
                i += 1
        yield clk.negedge
        axis_raw.tvalid.next = 0

    @instance
    def read_stim():
        yield reset.posedge
        yield delay(601)
        yield clk.negedge
        axis_sum.tready.next = 1
        while True:
            yield clk.negedge

            if axis_sum.tvalid:
                print(axis_sum.tdata)

            if axis_sum.tlast == 1:
                break
        for i in range(10):
            yield clk.negedge
        raise StopSimulation()

    uut = max_pool(clk, reset, axis_raw, axis_sum, width=6, height=4, depth=2, window_width=3, window_height=2)
    # uut = max_pool(clk, reset, axis_raw, axis_sum, size=(6, 4, 2), pool_size=(3,2)width=6, height=4, depth=2, window_width=3, window_height=2)

    if vhdl_output_path is not None:
        uut.convert(hdl='VHDL', path=vhdl_output_path, initial_values=True)
    return instances()


if __name__ == '__main__':
    trace_save_path = '../out/testbench/'
    vhdl_output_path = '../out/vhdl/'
    os.makedirs(os.path.dirname(trace_save_path), exist_ok=True)
    os.makedirs(os.path.dirname(vhdl_output_path), exist_ok=True)

    tb = testbench(vhdl_output_path)
    tb.config_sim(trace=True, directory=trace_save_path, name='my_sum_tb')
    tb.run_sim()